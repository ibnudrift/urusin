// libs.....
import React, {Component} from "react";
import {
    Dimensions, View,
    Text,
    TouchableOpacity,
    StyleSheet, Image,
    Platform
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {colors} from "../../utils/colors";
import {VectorIconName} from "../../utils/Strings";
import {HomeImages, BottomMenu, Extras} from "../../utils/imagesPath";
import RenderIf from "../common/RenderIf";

const dimensions = Dimensions.get('window');

type Props = {
    isDrawer: boolean,
    leftIconName: string,
    leftIconSize: number,
    rightIconName: string,
    rightIconSize: number,
    titleName: string,
}

export default class ToolBar extends Component<Props> {
    static defaultProps = {
        isDrawer: true,
        leftIconName: 'arrow-left',
        leftIconSize: 10,
        rightIconName: VectorIconName.bell_icon,
        rightIconSize: 25,
        titleName: ''
    }

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View
                style={[styles.container, {justifyContent: this.props.titleName === '' ? 'space-between' : 'flex-start'}]}>
                <TouchableOpacity
                    hitSlop={styles.hitSlopArea}
                    onPress={() => (this.props.isDrawer) ? Actions.drawerOpen() : Actions.pop()}>
                    <Image
                        source={BottomMenu.BUTTON_ARROW}
                        resizeMode={'contain'}
                        style={{height: 35, width: 35}}/>
                </TouchableOpacity>
                {this.props.titleName !== '' ? <Text
                    allowFontScaling={false}
                    style={styles.text}>{this.props.titleName}</Text> : <Image
                    style={{height: 25, width: 100}}
                    source={HomeImages.LOGO_ICON}
                    resizeMode={'contain'}
                />}
                <RenderIf condition={this.props.titleName === ''}>
                    <Image
                        style={{height: 25, width: 25}}
                        // source={NotificationBells.WHITE_NOTIFICATION_BELL}
                        resizeMode={'contain'}
                    />
                </RenderIf>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.app_theme_color,
        width: dimensions.width,
        flexDirection: 'row',
        alignItems: 'center',
        height: dimensions.width * 0.20,
        padding: 10,
        // marginTop: Platform.OS === 'ios'? 30 : 0
    },
    text: {
        color: colors.white_color,
        fontSize:18,
        marginLeft: 10,
    }, hitSlopArea: {
        top: 20,
        bottom: 20,
        left: 50,
        right: 50
    }
});
