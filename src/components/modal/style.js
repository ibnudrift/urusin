import {StyleSheet, Platform, Dimensions} from 'react-native';
import {colors} from '../../utils/colors'

const windows = Dimensions.get('window')

module.exports = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(52, 52, 52, 0.8)'
    },
    container: {
        justifyContent: 'center',
        backgroundColor: colors.blue_air_color,
        flexDirection: 'column',
        alignContent: 'center',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
    },
    titleText: {
        color: colors.white_color,
        fontSize: 18,
        marginHorizontal: 5,
        fontWeight: 'bold'
    },
    containerRadio: {
        flexDirection: 'row',
        margin: 10,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    radioBtn: {
        height: 16,
        width: 16,
        borderRadius: 8,
        borderColor: '#ebebeb',
        borderWidth: 2
    },
    
    textHead2: {fontSize: 16, color: colors.dark_grey},

})
