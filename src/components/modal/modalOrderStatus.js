import React, { Component } from 'react';
import { View, Text, Modal, Image, Dimensions, SafeAreaView, TouchableOpacity,} from 'react-native';
import propTypes from "prop-types";
import {colors} from '../../utils/colors'
import { ICON_SOSMED } from '../../utils/imagesPath';
import styles from './style';
import ModalComp from '../componentHistory/index'


const dimension = Dimensions.get('window');
class modalOrderStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
        visible: false,
        value :'',
    };
  }
    static propTypes = {
        visibleModal: propTypes.any,
        value: propTypes.any,
        data: propTypes.any,
        onClick: propTypes.func,
        closeModal: propTypes.func,
    };

    componentDidMount(){
        this.setState({visible: this.props.visibleModal});
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            visible: nextProps.visibleModal,
            value: nextProps.value,
            data: nextProps.data
        });
    }

  render() {
    return (
        <Modal backdropColor={'green'}
                backdropOpacity={1}
                visible={this.state.visible}
                animationType={'slide'}
                transparent={true}>

            <SafeAreaView style ={styles.mainContainer}>
                <View style={styles.container}>
                    <View style={{paddingHorizontal: 20}}>
                       <TouchableOpacity 
                            onPress={()=>this.setState({visible: false})}
                            style={{flexDirection: 'column', alignSelf: 'center',justifyContent: 'center',alignItems: 'center',}}>
                            <Image
                                source={ICON_SOSMED.ICON_MODAL} 
                                style={{ width: 85, height:25 }}
                                resizeMode={'stretch'}    
                            /> 
                            <Text style={styles.titleText}>{'Order Status'}</Text>
                        </TouchableOpacity>
                    </View>  
                    <ModalComp 
                        dateOrder={'05 Oct 2019'}
                        orderType={'Regular Laundry'}
                        company={'Ayo Laundry'}
                        RightText={'4.7'}
                    />
                    <View style={{ marginHorizontal:45, flexDirection: 'row',alignItems: 'center', justifyContent:'space-between', marginVertical:10}}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color:colors.white_color }}>Layanan</Text>
                        <View style={{ justifyContent: 'center', alignItems: 'center', height:35, width:dimension.width * 0.40, borderRadius: 30, borderColor: colors.white_color, borderWidth: 0.5, backgroundColor:colors.transparent }}>
                            <Text style={{ fontWeight: '100', color:colors.white_color }}>Cuci Basah</Text>
                        </View>
                    </View>
                    <View style={{ marginHorizontal:45, flexDirection: 'row',alignItems: 'center', justifyContent:'space-between', marginVertical:10}}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color:colors.white_color }}>Harga</Text>
                        <View style={{ justifyContent: 'center', alignItems: 'center', height:35, width:dimension.width * 0.40, borderRadius: 30, borderColor: colors.white_color, borderWidth: 0.5, backgroundColor:colors.transparent }}>
                            <Text style={{ fontWeight: '100', color:colors.white_color }}>Rp 3.500</Text>
                        </View>
                    </View>
                    <View style={{ marginHorizontal:45, flexDirection: 'row',alignItems: 'center', justifyContent:'space-between', marginVertical:10}}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color:colors.white_color }}>Jumlah</Text>
                        <View style={{ justifyContent: 'center', alignItems: 'center', height:35, width:dimension.width * 0.40, borderRadius: 30, borderColor: colors.white_color, borderWidth: 0.5, backgroundColor:colors.transparent }}>
                            <Text style={{ fontWeight: '100', color:colors.white_color }}>2</Text>
                        </View>
                    </View>
                    <View style={{ marginHorizontal:45, flexDirection: 'row',alignItems: 'center', justifyContent:'space-between', marginVertical:10}}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color:colors.white_color }}>Total</Text>
                        <View style={{ justifyContent: 'center', alignItems: 'center', height:35, width:dimension.width * 0.40, borderRadius: 30, borderColor: colors.white_color, borderWidth: 0.5, backgroundColor:colors.transparent }}>
                            <Text style={{ fontWeight: 'bold', color:colors.white_color }}>Rp 7.000</Text>
                        </View>
                    </View>
                    <TouchableOpacity style={{ backgroundColor: colors.lemonYellow, justifyContent:'center', alignItems: 'center', borderRadius:30, margin:15, height:50}}>
                        <Text style={{ fontSize:16, fontWeight:'bold', color:colors.white_color, paddingLeft: 10,  }}>Menunggu Konfirmasi</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        </Modal>
    );
  }
}

export default modalOrderStatus;
