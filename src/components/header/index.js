import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TextInput} from 'react-native';
import { HomeImages, BottomMenu, Extras } from '../../utils/imagesPath';
import { colors } from '../../utils/colors'

dimension = Dimensions.get('window')
class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{ backgroundColor:colors.new_theme, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
        <Image source={HomeImages.LOGO_ICON} 
            style={{ width: dimension.width * 0.35, height:dimension.height * 0.10 }} 
            resizeMode={'contain'}/>
        <View style={{ borderRadius: 30, width:dimension.width * 0.59, flexDirection: 'row', justifyContent: 'space-between',alignItems: 'center', borderColor: colors.white_color, borderWidth: 1, marginHorizontal: 20,}}>
          <TextInput 
            placeholder=" Cari Layanan "></TextInput>
          <Image source={HomeImages.SEARCH} style={{ width:25, height:25, marginHorizontal:15}} resizeMode={'contain'}/>
        </View>
      </View>
    );
  }
}

export default Header;
