const HomeImages = {
    HOME : require('../assets/Images/home.png'),
    USER : require('../assets/Images/user.png'),
    MAIL : require('../assets/Images/email.png'),
    MENU_ICON : require('../assets/Images/menuBar.png'),
    SEARCH : require('../assets/Images/searcIcon.png'),
    LOGO_ICON : require('../assets/Images/image0.png'),
    LANDING : require('../assets/Images/landingPage.png'),
    PROFILE : require('../assets/Images/userRegist.png'),
    ARROWRIGHT_YELLOW: require('../assets/Images/arrowRightYellow.png'),
    SPLASH_LOGO : require('../assets/Images/image0.png'),
    LOGO_LOGIN : require('../assets/Images/image0.png'),
}

const ICON_SOSMED ={
    TWITTER: require('../assets/Images/twiter.png'),
    FB: require('../assets/Images/faceook-icon-2.jpg'),
    GOOGLE: require('../assets/Images/googleIcon.png'),
    LINE: require('../assets/Images/line.png'),
    ICON_MODAL: require('../assets/Images/iconModal.png'),
}
const BottomMenu = {
    BUTTON_ARROW : require('../assets/Images/backButton.png'),
    PLUS: require('../assets/Images/plus.png'),
    MINUS: require('../assets/Images/minus.png'),
    ARROWDOUBLE_RIGHT : require('../assets/Images/doubleArrow.png'),
    ARROWDOUBLE_LEFT : require('../assets/Images/arrrowDoubleLeft.png'),
}

export {
  HomeImages,
  ICON_SOSMED,
  BottomMenu,
}