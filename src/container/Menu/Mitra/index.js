import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, ScrollView} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Header from '../../../components/header';
import Footer from '../../../components/footerTab';
import {colors} from '../../../utils/colors'
import InputModal from '../../../components/modal/modalInputOrder'
class Mitra extends Component {
  constructor(props) {
    super(props);
    this.state = {
      InputOrderModal: false,
      order_value:'',
    };
  }
  triggerStatusOrderModal() {
    this.setState({
      InputOrderModal: true,
    });
  }
  closeSortByModal() {
      this.setState({
          InputOrderModal: false
      })
  }

  render() {
    return (
      <View style={{ flex:1, backgroundColor: colors.white_color, }}>
        <Header/>
        <View style={{ justifyContent: 'center', alignItems: 'center', padding:5 }}>
          <Text style={{ fontSize: 24, fontWeight: 'bold', color:colors.Text_color }}> Mitra Kami </Text>
        </View>
        <ScrollView>
            <View style={{ margin:10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                <TouchableOpacity 
                      onPress={() => this.setState({ InputOrderModal: true })}
                      style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
                </TouchableOpacity>
                <TouchableOpacity 
                      onPress={() => this.setState({ InputOrderModal: true })}
                      style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
                </TouchableOpacity>
                <TouchableOpacity 
                      onPress={() => this.setState({ InputOrderModal: true })}
                      style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
                </TouchableOpacity>
            </View>
                <View style={{marginBottom:20, margin:10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                <TouchableOpacity 
                      onPress={() => this.setState({ InputOrderModal: true })}
                      style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
                </TouchableOpacity>
                <TouchableOpacity 
                      onPress={() => this.setState({ InputOrderModal: true })}
                      style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
                </TouchableOpacity>
                <TouchableOpacity 
                      onPress={() => this.setState({ InputOrderModal: true })}
                      style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
                </TouchableOpacity>
            </View>
            <View style={{ margin:10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                <TouchableOpacity 
                      onPress={() => this.setState({ InputOrderModal: true })}
                      style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
                </TouchableOpacity>
                <TouchableOpacity 
                      onPress={() => this.setState({ InputOrderModal: true })}
                      style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
                </TouchableOpacity>
                <TouchableOpacity 
                      onPress={() => this.setState({ InputOrderModal: true })}
                      style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
                </TouchableOpacity>
            </View>
            <View style={{marginBottom:20, margin:10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                <TouchableOpacity 
                      onPress={() => this.setState({ InputOrderModal: true })}
                      style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
                </TouchableOpacity>
                <TouchableOpacity 
                      onPress={() => this.setState({ InputOrderModal: true })}
                      style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
                </TouchableOpacity>
                <TouchableOpacity 
                      onPress={() => this.setState({ InputOrderModal: true })}
                      style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
                </TouchableOpacity>
            </View>
        </ScrollView>
        <Footer/>
        <InputModal
            visibleModal={this.state.InputOrderModal}
            value={''}
            onClick={(param) => { alert(param), this.setState({ InputOrderModal: false }) }} />
      </View>
    );
  }
}

export default Mitra;
