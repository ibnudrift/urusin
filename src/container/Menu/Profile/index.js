import React, { Component } from 'react';
import { View, Text, Dimensions, ScrollView, TouchableOpacity } from 'react-native';
import {Actions} from 'react-native-router-flux'
import {colors} from '../../../utils/colors'
import ToolBar from '../../../components/toolBar/toolbarHome'

const dimension=Dimensions.get('window')
class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{ flex:1, backgroundColor: colors.white_color, }}>
        <ToolBar isDrawer={false} titleName={'Account'}/>
        <View style={{ borderBottomWidth: 4, borderBottomColor: colors.blue_air_color, }}>
          <View style={{ justifyContent: 'space-between', flexDirection: 'row',alignItems: 'center', paddingTop:15,paddingBottom: 5, marginHorizontal: 15, }}>
            <Text style={{ fontWeight: '600', fontSize: 16, color:colors.black }}> Jhony gudel </Text>
            <Text style={{ fontSize: 14, color:colors.blue_air_color }}> Edit </Text>
          </View>
          <View style={{  padding:5, marginHorizontal: 15, }}>
            <Text>jhony@email.com</Text>
            <Text>081218273791</Text>
          </View>
        </View>
        
        <View style={{ borderBottomWidth: 4, borderBottomColor: colors.blue_air_color, }}>
          <View style={{ marginHorizontal: 15, }}>
            <Text style={{paddingVertical: 7, fontWeight: '200', fontSize: 14, color:colors.black }}> Enter Promo Code</Text>
            <Text style={{paddingVertical: 7, fontSize: 14, color:colors.black }}> My Voucher </Text>
            <Text style={{paddingVertical: 7, fontWeight: '200', fontSize: 14, color:colors.black }}>Change Language</Text>
          </View>
        </View>

        <View style={{ flex:1}}>
          <View style={{ marginHorizontal: 15, }}>
            <Text style={{paddingVertical:7, fontSize: 14, color:colors.black }}> Help</Text>
            <Text style={{paddingVertical:7, fontSize: 14, color:colors.black }}> Terms Of Service </Text>
            <Text style={{paddingVertical:7, fontSize: 14, color:colors.black }}> Logged in Device</Text>
            <Text style={{paddingVertical:7, fontSize: 14, color:colors.black }}> Privacy Policy</Text>
          </View>
        </View>
        <TouchableOpacity 
            onPress={()=> Actions.Login()}
            style={{ backgroundColor: colors.lemonYellow, borderRadius: 30, margin:15, justifyContent: 'center',alignItems:'center', marginBottom:10 }}>
          <Text style={{ fontWeight:'bold', fontSize:18,color:colors.Text_color, paddingVertical:10 }}>Logout</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Profile;
