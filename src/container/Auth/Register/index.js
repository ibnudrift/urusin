import React, { Component } from 'react';
import { View, Text, TouchableOpacity,TextInput, Image, Dimensions} from 'react-native';
import { Thumbnail} from 'native-base'
import{Actions} from 'react-native-router-flux';

import { colors } from '../../../utils/colors';
import {HomeImages, ICON_SOSMED} from '../../../utils/imagesPath';

const dimension=Dimensions.get('window')
class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{ flex:1, backgroundColor: colors.app_theme_color, }}>
          <View style={{
                    alignSelf: 'center', 
                    borderRadius:3000, 
                    backgroundColor: colors.white_color, 
                    justifyContent: 'center', 
                    alignItems: 'center', 
                    width:100,
                    marginTop: dimension.height *0.13,
                    marginBottom: 10,
                  }}>
            <Image large source={HomeImages.PROFILE} />          
          </View>

        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
          <Text style={{ fontSize: 24,color:colors.white_color}}>CREATE </Text>
          <Text style={{ fontSize: 24, fontWeight: 'bold', color:colors.white_color}}>ACCOUNT</Text>
        </View>
        <Image source={ICON_SOSMED.LINE} style={{width: dimension.width*0.45,height:5, marginVertical: 5, alignSelf: 'center',}}/>
        <View style={{ 
                justifyContent: 'center', 
                flexDirection: 'row', 
                borderColor: colors.white_color, 
                borderWidth: 1, 
                alignItems: 'center', 
                borderRadius: 30,
                marginVertical:7,
                marginHorizontal:45
              }}>
          <TextInput placeholder='your nickname'/>
        </View>
        <View style={{ 
                justifyContent: 'center', 
                flexDirection: 'row', 
                borderColor: colors.white_color, 
                borderWidth: 1, 
                alignItems: 'center', 
                borderRadius: 30,
                marginVertical:7,
                marginHorizontal:45
              }}>
          <TextInput placeholder='your email'/>
        </View>
        <View style={{ 
                justifyContent: 'center', 
                flexDirection: 'row', 
                borderColor: colors.white_color, 
                borderWidth: 1, 
                alignItems: 'center', 
                borderRadius: 30,
                marginVertical:7,
                marginHorizontal:45
              }}>
          <TextInput placeholder='your password'/>
        </View>
        <View style={{ 
                justifyContent: 'center', 
                flexDirection: 'row', 
                borderColor: colors.white_color, 
                borderWidth: 1, 
                alignItems: 'center', 
                borderRadius: 30,
                marginVertical:7,
                marginHorizontal:45
              }}>
          <TextInput placeholder='confirmation password'/>
        </View>

        <TouchableOpacity onPress={()=> Actions.HomePage()} style={{ justifyContent: 'center', alignItems: 'center', marginTop: 15, }}>
          <Image source={HomeImages.ARROWRIGHT_YELLOW} style={{ width:40, height:40}} resizeMode={'contain'}/>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Register;
