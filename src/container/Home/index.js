import React, { Component } from 'react';
import { View, Text, Dimensions, Image, ScrollView, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { colors } from '../../utils/colors';
import Header from '../../components/header/index';
import Footer from '../../components/footerTab/index'

const dimension = Dimensions.get('window')
class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{ flex:1, backgroundColor: colors.white_color, }}>
        <Header/>
        <ScrollView>
          <View style={{ justifyContent: 'center',alignItems: 'center', padding:5 }}>
            <Text style={{ fontSize:36, fontWeight: 'bold', color:colors.Text_color }}> Layanan </Text>
          </View>
          
          <View style={{ marginHorizontal: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center',}}>
            <TouchableOpacity 
                onPress={()=> Actions.Laundry()}
                style={{ 
                    borderRadius: 5, 
                    marginHorizontal:5, 
                    justifyContent: 'center',
                    alignItems: 'center', 
                    marginVertical:10 
                  }}>
              <View style={{ backgroundColor: colors.blue_air_color,width:dimension.width * 0.45, height: dimension.height * 0.25, borderRadius:5 }}></View>
              <Text style={{ fontSize: 16, color:colors.blue_air_color }}> Laundry Service </Text>
            </TouchableOpacity>
            <TouchableOpacity 
                onPress={()=> Actions.CarWash()}
                style={{ 
                    borderRadius: 5, 
                    marginHorizontal:5,
                    justifyContent: 'center',
                    alignItems: 'center', 
                    marginVertical:10  
                  }}>
              <View style={{ backgroundColor: colors.blue_air_color,width:dimension.width * 0.45, height: dimension.height * 0.25, borderRadius:5}}></View>
              <Text style={{ fontSize: 16, color:colors.blue_air_color }}> Car Repair Service</Text>
            </TouchableOpacity>
          </View>

          <View style={{ justifyContent: 'space-between',alignItems: 'center',flexDirection: 'row', padding:5, marginHorizontal:15 }}>
            <Text style={{ fontSize:24, fontWeight: 'bold', color:colors.Text_color }}> Mitra Unggulan </Text>
            <TouchableOpacity onPress={()=> Actions.Mitra()}>
              <Text style={{ fontSize:16, fontWeight: '300', color:colors.Text_color }}> Lihat Semua </Text>
            </TouchableOpacity>
          </View>

        
          <View style={{ margin:10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
            <TouchableOpacity style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
            </TouchableOpacity>
            <TouchableOpacity style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
            </TouchableOpacity>
            <TouchableOpacity style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
            </TouchableOpacity>
          </View>
          <View style={{marginBottom:20, margin:10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
            <TouchableOpacity style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
            </TouchableOpacity>
            <TouchableOpacity style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
            </TouchableOpacity>
            <TouchableOpacity style={{ width:dimension.width * 0.25, height:dimension.width * 0.25, backgroundColor: colors.blue_air_color,borderRadius:5,marginHorizontal:10 }}>
            </TouchableOpacity>
          </View>
          <View style={{ margin:10, height:dimension.height *0.10 }}/>
        </ScrollView>
        
        <Footer/>
      </View>
    );
  }
}

export default HomePage;
