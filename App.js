import React,{Component} from 'react';
import {Root} from 'native-base';
import AppRouter from './src/router/index'

class App extends Component {
  render() {
      return (
        <Root>
          <AppRouter/>
        </Root>
    );
  }
}

export default App;